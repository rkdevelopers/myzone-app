<?php
// features/bootstrap/FeatureContext.php

use App\Entity\User;
use App\Kernel;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behatch\Context\RestContext;
use Doctrine\Common\Persistence\ObjectManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;

class FeatureContext implements Context, SnippetAcceptingContext
{
    private $doctrine;
    private $jwtManager;
    private $kernel;

    public function __construct(ObjectManager $doctrine, JWTManager $jwtManager, Kernel $kernel)
    {
        $this->doctrine = $doctrine;
        $this->jwtManager = $jwtManager;
        $this->kernel = $kernel;
    }

    /**
     * @BeforeScenario
     * @login
     *
     * @see https://symfony.com/doc/current/security/entity_provider.html#creating-your-first-user
     */
    public function login(BeforeScenarioScope $scope)
    {
        $user = new User('admin20');
        $user->setPassword('ATestPassword');

        $this->doctrine->persist($user);
        $this->doctrine->flush();

        $token = $this->jwtManager->create($user);

        $this->restContext = $scope->getEnvironment()->getContext(RestContext::class);
        $this->restContext->iAddHeaderEqualTo('Authorization', "Bearer $token");
    }

    /**
     * @AfterScenario
     * @logout
     */
    public function logout() {
        $this->restContext->iAddHeaderEqualTo('Authorization', '');
    }
}
