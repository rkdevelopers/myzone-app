<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191101122549 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_rating (id INT AUTO_INCREMENT NOT NULL, rating DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_rating_product (product_rating_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_636F83F96492594B (product_rating_id), INDEX IDX_636F83F94584665A (product_id), PRIMARY KEY(product_rating_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_rating_user (product_rating_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_E5C4DBD36492594B (product_rating_id), INDEX IDX_E5C4DBD3A76ED395 (user_id), PRIMARY KEY(product_rating_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_rating_product ADD CONSTRAINT FK_636F83F96492594B FOREIGN KEY (product_rating_id) REFERENCES product_rating (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_rating_product ADD CONSTRAINT FK_636F83F94584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_rating_user ADD CONSTRAINT FK_E5C4DBD36492594B FOREIGN KEY (product_rating_id) REFERENCES product_rating (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_rating_user ADD CONSTRAINT FK_E5C4DBD3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE products');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_rating_product DROP FOREIGN KEY FK_636F83F96492594B');
        $this->addSql('ALTER TABLE product_rating_user DROP FOREIGN KEY FK_E5C4DBD36492594B');
        $this->addSql('CREATE TABLE products (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, price DOUBLE PRECISION NOT NULL, rating DOUBLE PRECISION NOT NULL, updated_at DATETIME DEFAULT NULL, variations JSON DEFAULT NULL COMMENT \'(DC2Type:json_array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE product_rating');
        $this->addSql('DROP TABLE product_rating_product');
        $this->addSql('DROP TABLE product_rating_user');
    }
}
