<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191103141650 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE products');
        $this->addSql('DROP TABLE users');
        $this->addSql('ALTER TABLE product_rating DROP INDEX UNIQ_BAF567864584665A, ADD INDEX IDX_BAF567864584665A (product_id)');
        $this->addSql('ALTER TABLE product_rating DROP INDEX UNIQ_BAF56786A76ED395, ADD INDEX IDX_BAF56786A76ED395 (user_id)');
        $this->addSql('ALTER TABLE product_rating CHANGE product_id product_id INT NOT NULL, CHANGE user_id user_id INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE products (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, price DOUBLE PRECISION NOT NULL, rating DOUBLE PRECISION NOT NULL, updated_at DATETIME DEFAULT NULL, variations JSON DEFAULT NULL COMMENT \'(DC2Type:json_array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(25) NOT NULL COLLATE utf8mb4_unicode_ci, password VARCHAR(500) NOT NULL COLLATE utf8mb4_unicode_ci, is_active TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_1483A5E9F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE product_rating DROP INDEX IDX_BAF567864584665A, ADD UNIQUE INDEX UNIQ_BAF567864584665A (product_id)');
        $this->addSql('ALTER TABLE product_rating DROP INDEX IDX_BAF56786A76ED395, ADD UNIQUE INDEX UNIQ_BAF56786A76ED395 (user_id)');
        $this->addSql('ALTER TABLE product_rating CHANGE product_id product_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
    }
}
