<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"layout:read"}},
 *     denormalizationContext={"groups"={"layout:write"}},
 *     collectionOperations={"post"={"path"="/product"}, "get"={"path"="/products"}},
 *     itemOperations={"get"={"path"="/product/{id}"}, "put"={"path"="/product/{id}"}, "delete"={"path"="/product/{id}"}, "patch"={"path"="/product/{id}"}}
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Groups({"layout:read", "layout:write"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"layout:read", "layout:write"})
     * @ApiFilter(SearchFilter::class, strategy="partial")
     * @Assert\NotBlank(message="Name can not be empty!")
     *
     */
    private $name;

    /**
     * @ORM\Column(type="float", length=20)
     * @Groups({"layout:read", "layout:write"})
     * @ApiFilter(SearchFilter::class, strategy="partial")
     * @Assert\NotBlank(message="Price can not be empty!")
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"layout:read"})
     * @ApiFilter(SearchFilter::class, strategy="partial")
     * @Assert\Range(
     *      min = 0,
     *      max = 5)
     */
    private $rating;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @ApiFilter(SearchFilter::class, strategy="partial")
     * @Groups({"layout:read", "layout:write"})
     */
    private $updated_at;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     * @Groups({"layout:read", "layout:write"})
     * @ApiFilter(SearchFilter::class, strategy="partial")
     */
    private $variations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductRating", mappedBy="product", cascade={"all"})
     * @Groups({"none"})
     */
    private $productRatings;

    public function __construct()
    {
        $this->updated_at = new \DateTime('now');
        $this->productRatings = new ArrayCollection();
        $this->rating = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating): void
    {
        $this->rating = $rating;
    }


    /**
     * @return mixed
     */
    public function getVariations()
    {
        return $this->variations;
    }

    /**
     * @param mixed $variations
     */
    public function setVariations($variations): void
    {
        $this->variations = $variations;
    }

    /**
     * @return Collection|ProductRating[]
     */
    public function getProductRatings(): Collection
    {
        return $this->productRatings;
    }

    public function addProductRating(ProductRating $productRating): self
    {
        if (!$this->productRatings->contains($productRating)) {
            $this->productRatings[] = $productRating;
            $productRating->setProduct($this);
        }

        return $this;
    }

    public function removeProductRating(ProductRating $productRating): self
    {
        if ($this->productRatings->contains($productRating)) {
            $this->productRatings->removeElement($productRating);
            // set the owning side to null (unless already changed)
            if ($productRating->getProduct() === $this) {
                $productRating->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }
}
