<?php

namespace App\Service;

use App\Entity\Product;
use App\Entity\ProductRating;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class RatingActionHandler {

    private $em;
    private $tokenStorage;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function saveRating(Request $request)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $data = $request->getContent();
        $json = json_decode($data, true);

        if ($json['product'] != null && $json['rating'] != null) {
            return $this->validateRating($json, $user);
        } else {
            return ('You need to put body parameters "product" and "rating"!');
        }
    }

    /**
     * @param $json
     * @param $user
     * @return string
     */
    public function validateRating($json, $user)
    {
        $product = $this->em->getRepository(Product::class)->find($json['product']);

        if ($json['rating'] >= 0 && $json['rating'] <= 5) {
            $isRated = $this->em->getRepository(ProductRating::class)->findOneBy([
                'product' => $product,
                'user' => $user
            ]);

            if (!$isRated) {
                $rating = new ProductRating();
                $rating->setUser($user);
                $rating->setProduct($product);
                $rating->setRating($json['rating']);
                $this->em->persist($rating);
                $this->em->flush();

                $ratingValue = $this->refreshProductRating($product);
                $product->setRating($ratingValue);
                $this->em->flush();

            } else {
                return('You have already rated this product. You can not rate it twice!');
            }
            return ('You have successfully rated the product!');
        } else {
            return ('Rating must be between 0 and 5!');
        }
    }

    public function refreshProductRating($product)
    {
        $productRatings = $this->em->getRepository(ProductRating::class)->findBy([
            'product' => $product
        ]);
        $numberOfRatings = count($productRatings);
        if ($numberOfRatings > 0) {
            $value = 0;
            foreach ($productRatings as $rating) {
                $value += $rating->getRating();
            }

            $productRating = $value / $numberOfRatings;

            return $productRating;
        } else {
            return 0;
        }
    }
}