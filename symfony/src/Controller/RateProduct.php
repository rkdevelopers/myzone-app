<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductRating;
use App\Entity\User;
use App\Service\RatingActionHandler;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use function MongoDB\BSON\fromJSON;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class RateProduct extends AbstractController
{
    private $ratingActionHandler;

    public function __construct(RatingActionHandler $ratingActionHandler)
    {
        $this->ratingActionHandler = $ratingActionHandler;
    }

    public function __invoke(Request $request)
    {
        $saveRating = $this->ratingActionHandler->saveRating($request);

        return new JsonResponse($saveRating);
    }
}
