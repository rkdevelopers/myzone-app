## Symfony 4, REST API with API Platform (Docker container)

### Requirements

Install docker and docker-compose

    sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    sudo apt-get update
    sudo apt-get remove docker docker-engine docker.io
    sudo apt install docker.io
     
### Development environment setup

Clone repository

    git clone git@bitbucket.org:rkdevelopers/myzone-app.git
    
Set logs permissions

    cd myzone-app/
    sudo chmod -R 777 logs/
    
Start Docker's containers

    docker-compose up
    
List active containers

    docker ps
    
Take ID from your _php container, run Composer and then migrations

    docker container exec <your_php_container_id> composer install
    docker container exec <your_php_container_id> ./bin/console doctrine:migrations:migrate --no-interaction
    docker container exec <your_php_container_id> sh reload.sh
    
Update hosts file

    echo '127.0.0.1 myzone.local' | sudo tee --append /etc/hosts >/dev/null
    
Generate authentication keys for JWT (from project root)

    cd symfony/
    mkdir config/jwt
    openssl genrsa -out config/jwt/private.pem -aes256 4096
    openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
    sudo chmod -R 777 config/jwt
    
Import test database (Read what test db contains in section "Testing database")

    cat backup.sql | sudo docker exec -i <your_mysql_container_id> /usr/bin/mysql -u root myzone-rest-api
    
IMPORTANT NOTE

    If your containers does not start, you need to stop web server and database server on your local machine or reconfigure ports for MySQL and Apache containers.

#### Available endpoints, usage

List all products

    Request: GET
    Endpoint: http://myzone.local/api/products
    Headers:  Content-Type: application/json, Authorization: Bearer <auth_token>
             
    Optional parameters:
        pagination: true/false (true by default)
        page: integer/number
        name: string (search param)
        price: float (search param)
        rating: float (search param)
        updated_at: date (search param)
        
    Example of using optional parameter:
        Endpoint: http://myzone.local/api/products?pagination=false
                  http://myzone.local/api/products?name=Samsung
                  http://myzone.local/api/products?page=2
        Headers:  Content-Type: application/json, Authorization: Bearer <auth_token>
                  
Create a new product

    Request: POST
    Endpoint: http://myzone.local/api/product
    Headers: Content-Type: application/json, Authorization: Bearer <auth_token>
    Body: {"name": "string", "price": "float"} 
    
Update/edit product

    Request: PATCH
    Endpoint: http://myzone.local/api/product/{id}
    Headers: Content-Type: application/merge-patch+json, Authorization: Bearer <auth_token>
    Body: {"name": "string", "price": "float"} or
    Body: {"name": "string"} or
    Body: {"price": "float"} 
    
Delete product

    Request: DELETE
    Endpoint: http://myzone.local/api/product/{id}
    Headers: Content-Type: application/ld+json, Authorization: Bearer <auth_token>
             
Rate the product

    Request: POST
    Endpoint: http://myzone.local/api/product/rate
    Headers: Content-Type: application/json, Authorization: Bearer <auth_token>
    Body: {"product": "integer/id", "rating": "float"}

#### Testing database

Database contains 2 users:
   - username: api / password: apitest
   - username: api2 / password: apitest2
   
Use existing users to get authentication token or feel free to create a new user by making a proper request.
Database also contains 6 demo products for list/edit/remove/search. Feel free to create more products by making a proper request.


#### Usable commands

Register a new user

    curl -X POST http://myzone.local/register -d _username=api -d _password=apitest
    
Get the authentication token

    curl -X POST -H "Content-Type: application/json" http://myzone.local/login_check -d '{"username":"api","password":"apitest"}'
    
Start Docker containers from the root of the project

    docker-compose up
    
Build Docker containers from the root of the project

    docker-compose build
    
List running Docker containers from the root of the project

    docker ps
    
Execute Composer or Symfony console commands on a Docker container

    Pattern: docker container exec <php_container_id> <command_you_want_to_execute>
    Example: docker container exec 46454688521869 ./bin/console cache:clear

Login to MySQL Docker container

    docker exec -it <mysql_container_id> bash -l
    
Dump and restore DB on MySQL Docker container

    # Backup
    docker exec <your_mysql_container_id> /usr/bin/mysqldump -u root myzone-rest-api > backup.sql
    
    # Restore
    cat backup.sql | docker exec -i <your_mysql_container_id> /usr/bin/mysql -u root myzone-rest-api
    
During the auth keys generation, you will be asked for passphrase. Update .env file with the same passphrase under the variable "JWT_PASSPHRASE"     
    
    

    









